/*
 * HydroPlugin.c
 *
 *  Created on: Oct 23, 2015
 *      Author: bazow
 */

#include <stdlib.h>
#include <stdio.h> // for printf

// for timing
#include <ctime>
#include <iostream>

#include "edu/osu/rhic/harness/hydro/HydroPlugin.h"
#include "edu/osu/rhic/trunk/hydro/DynamicalVariables.h"
#include "edu/osu/rhic/harness/lattice/LatticeParameters.h"
#include "edu/osu/rhic/harness/ic/InitialConditionParameters.h"
#include "edu/osu/rhic/harness/hydro/HydroParameters.h"
#include "edu/osu/rhic/harness/io/FileIO.h"
#include "edu/osu/rhic/trunk/ic/InitialConditions.h"
#include "edu/osu/rhic/trunk/hydro/FullyDiscreteKurganovTadmorScheme.h"
#include "edu/osu/rhic/trunk/hydro/EnergyMomentumTensor.h"
#include "edu/osu/rhic/trunk/eos/EquationOfState.h"

#define FREQ 1

void outputDynamicalQuantities(double t, const char *outputDir, void * latticeParams) {
/*
	output(e, t, outputDir, "e", latticeParams);
	output(u->ux, t, outputDir, "ux", latticeParams);	
	output(u->uy, t, outputDir, "uy", latticeParams);	
//	output(u->un, t, outputDir, "un", latticeParams);
	output(u->ut, t, outputDir, "ut", latticeParams);
//	output(q->ttt, t, outputDir, "ttt", latticeParams);	
//	output(q->ttn, t, outputDir, "ttn", latticeParams);
#ifdef PIMUNU	
	output(q->pixx, t, outputDir, "pixx", latticeParams);	
	output(q->pixy, t, outputDir, "pixy", latticeParams);	
	output(q->pixn, t, outputDir, "pixn", latticeParams);	
	output(q->piyy, t, outputDir, "piyy", latticeParams);	
	output(q->piyn, t, outputDir, "piyn", latticeParams);	
	output(q->pinn, t, outputDir, "pinn", latticeParams);	
#endif
#ifdef PI
	output(q->Pi, t, outputDir, "Pi", latticeParams);	
#endif
*/
}

void run(void * latticeParams, void * initCondParams, void * hydroParams, const char *rootDirectory, const char *outputDir) {
	struct LatticeParameters * lattice = (struct LatticeParameters *) latticeParams;
	struct InitialConditionParameters * initCond = (struct InitialConditionParameters *) initCondParams;
	struct HydroParameters * hydro = (struct HydroParameters *) hydroParams;

	/************************************************************************************\
	 * System configuration 
	/************************************************************************************/
	int nt = lattice->numProperTimePoints;
	int nx = lattice->numLatticePointsX;
	int ny = lattice->numLatticePointsY;
	int nz = lattice->numLatticePointsRapidity;
	int ncx = lattice->numComputationalLatticePointsX;
	int ncy = lattice->numComputationalLatticePointsY;
	int ncz = lattice->numComputationalLatticePointsRapidity;
	int nElements = ncx * ncy * ncz;

	double t0 = hydro->initialProperTimePoint;
	double dt = lattice->latticeSpacingProperTime;

	double e0 = initCond->initialEnergyDensity;

	double freezeoutTemperatureGeV = hydro->freezeoutTemperatureGeV;
	const double hbarc = 0.197326938;
	const double freezeoutTemperature = freezeoutTemperatureGeV/hbarc;
//	const double freezeoutEnergyDensity = e0*pow(freezeoutTemperature,4);
	const double freezeoutEnergyDensity = equilibriumEnergyDensity(freezeoutTemperature);
	printf("Grid size = %d x %d x %d\n", nx, ny, nz);
	printf("spatial resolution = (%.3f, %.3f, %.3f)\n", lattice->latticeSpacingX, lattice->latticeSpacingY, lattice->latticeSpacingRapidity);
	printf("freezeout temperature = %.3f [fm^-1] (eF = %.3f [fm^-4])\n", freezeoutTemperature, freezeoutEnergyDensity);

	// allocate memory
	allocateHostMemory(nElements);

	/************************************************************************************\
	 * Fluid dynamic initialization 
	/************************************************************************************/
	double t = t0;
	// generate initial conditions 
	setInitialConditions(latticeParams, initCondParams, hydroParams, rootDirectory);
	// Calculate conserved quantities
	setConservedVariables(t, latticeParams);
	// impose boundary conditions with ghost cells
	setGhostCells(q,e,p,u,latticeParams);

	/************************************************************************************\
	 * Evolve the system in time
	/************************************************************************************/
	int ictr = (nx % 2 == 0) ? ncx/2 : (ncx-1)/2;
	int jctr = (ny % 2 == 0) ? ncy/2 : (ncy-1)/2;
	int kctr = (nz % 2 == 0) ? ncz/2 : (ncz-1)/2;	
	int sctr = columnMajorLinearIndex(ictr, jctr, kctr, ncx, ncy);

	std::clock_t t1,t2;

	double totalTime = 0;
	int nsteps = 0;

	// evolve in time
	for (int n = 1; n <= nt+1; ++n) {
		// copy variables back to host and write to disk
		if ((n-1) % FREQ == 0) {
			printf("n = %d:%d (t = %.3f),\t (e, p) = (%.3f, %.3f) [fm^-4],\t (T = %.3f [GeV]),\t", 
				n - 1, nt, t, e[sctr], p[sctr], effectiveTemperature(e[sctr])*hbarc);
			outputDynamicalQuantities(t, outputDir, latticeParams);
			// end hydrodynamic simulation if the temperature is below the freezeout temperature
			if(e[sctr] < freezeoutEnergyDensity) {
				printf("\nReached freezeout temperature at the center.\n");
				break;
			}
		}

		t1 = std::clock();
		rungeKutta2(t, dt, q, Q, latticeParams, hydroParams);
		t2 = std::clock();
		double delta_time = (t2 - t1) / (double)(CLOCKS_PER_SEC / 1000);
		if ((n-1) % FREQ == 0) printf("(Elapsed time: %.3f ms)\n",delta_time);
		totalTime+=delta_time;
		++nsteps;

		setCurrentConservedVariables();

		t = t0 + n * dt;
	}
	printf("Average time/step: %.3f ms\n",totalTime/((double)nsteps));

	/************************************************************************************\
	 * Deallocate host memory
	/************************************************************************************/
	freeHostMemory();
}
